import java.util.Scanner;

public class StandardInputOutput {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter name: ");
        String name = sc.next();

        System.out.print("Enter Age: ");
        int age = sc.nextInt() ;

        System.out.println("Your name is: " + name);
        System.out.println("Your age is: " + age);

    }

}
