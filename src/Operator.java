public class Operator {

    public static void main(String[] args) {

        float f = 30.5F;
        f -= 40; // f = f - 40;
        System.out.println(" F= " + f);

        int i = 13 % 2;
        System.out.println("i = " + i);

        int x = 5;
        int y = 10;
        int z = 50;

        // && vs &
        if (x == 5 & ++y<10){
            System.out.println("if work aii");
        }
        System.out.println("&& y = " + y);

        // || vs |
        if (x != 1 | ++z>10) {
            System.out.println("if work aii");
        }
        System.err.println("z = " + z);

    }

}
