public class Main {

    public static void main(String[] args) {

        // Auto Boxing: convert Primitive -> Wrapper class
        Integer iWrapper = 9;

        // Unboxing: convert Wrapper Class -> Primitive
        int iPrimitive = iWrapper;

        System.out.println("iWrapper: " + iWrapper);
        System.out.println("iPrimitive: " + iPrimitive);

        // Promotion: Convert smaller type -> bigger type
        short s = 23;
        int i = s;

        // Casting
        double d = 384.0;
        short sCasting = (short)d;
        System.out.println("sCasting: " + sCasting);


    }
}
